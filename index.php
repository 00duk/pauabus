<?php
require __DIR__.'/vendor/autoload.php';

use Pauabus\CheapestPathCalculator;
use Pauabus\FastestPathCalculator;



/*
 *     D---9€--E
 *    / \       \
 *  14€  2€      6€
 *  /     \       \
 * A---9€--B--11€-C
 *  \     /      /
 *   7€ 10 €    /
 *    \ /      /
 *     F-----15€
 *
 */
$cityGraphWithPrices = [
  'Aachen' => ['Berlin' => 9, 'Dresden' => 14, 'Flensburg' => 7],
  'Berlin' => ['Aachen' => 9, 'Chemnitz' => 11, 'Dresden' => 2, 'Flensburg' => 10],
  'Chemnitz' => ['Berlin' => 11, 'Essen' => 6, 'Flensburg' => 15],
  'Dresden' => ['Aachen' => 14, 'Berlin' => 2, 'Essen' => 9],
  'Essen' => ['Chemnitz' => 6, 'Dresden' => 9],
  'Flensburg' => ['Aachen' => 7, 'Berlin' => 10, 'Chemnitz' => 15],
];

/*
 *     D---6h--E
 *    / \       \
 *  6.5h 2.5h   5.5h
 *  /     \       \
 * A---7h--B--3h--C
 *  \     /      /
 * 7.5h  4.5h   /
 *    \ /      /
 *     F-----7h
 *
 */
$cityGraphWithDuration = [
  'Aachen' => ['Berlin' => 7, 'Dresden' => 6.5, 'Flensburg' => 7.5],
  'Berlin' => ['Aachen' => 6.5, 'Chemnitz' => 3, 'Dresden' => 2.5, 'Flensburg' => 4.5],
  'Chemnitz' => ['Berlin' => 3, 'Essen' => 5.5, 'Flensburg' => 7],
  'Dresden' => ['Aachen' => 6.5, 'Berlin' => 2.5, 'Essen' => 6],
  'Essen' => ['Chemnitz' => 5.5, 'Dresden' => 6],
  'Flensburg' => ['Aachen' => 7.5, 'Berlin' => 5, 'Chemnitz' => 7],
];


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $cheapestPathCalculator = new CheapestPathCalculator($cityGraphWithPrices);
    $fastestPathCalculator = new FastestPathCalculator($cityGraphWithDuration);

    $path = $fastestPathCalculator->getPath($_POST["start"], $_POST["destination"]);
    $hours = $fastestPathCalculator->getTime($_POST["start"], $_POST["destination"]);

    echo 'Fastest from ' . $_POST["start"].' to '.$_POST["destination"].': ' . implode('->',$path) . ' ('.$hours.'hours)<br>';

    $price = $cheapestPathCalculator->getPrice($_POST["start"], $_POST["destination"]);
    echo 'Cheapest from ' . $_POST["start"].' to '.$_POST["destination"].': ' . implode('->',$path) . ' ('.$price.' €)';
}
?>


<html>
<body>

<h1>Pauabus booking</h1>

<form action="index.php" method="post">
    Start:
    <select name="start">
        <option value="Aachen">Aachen</option>
        <option value="Berlin">Berlin</option>
        <option value="Dresden">Dresden</option>
        <option value="Flensburg">Flensburg</option>
        <option value="Chemnitz">Chemnitz</option>
    </select><br>
    Destination: <select name="destination">
        <option value="Aachen">Aachen</option>
        <option value="Berlin">Berlin</option>
        <option value="Dresden">Dresden</option>
        <option value="Flensburg">Flensburg</option>
        <option value="Chemnitz">Chemnitz</option>
    </select><br>
    <input type="submit">
</form>

</body>
</html>
