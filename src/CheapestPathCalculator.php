<?php

namespace Pauabus;

class CheapestPathCalculator extends AbstractPathCalculator {
  public function getPrice($fromCity, $toCity) {
    return $this->getWeight($fromCity, $toCity);
  }
}
