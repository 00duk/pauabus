<?php

namespace Pauabus;

use Fisharebest\Algorithm\Dijkstra;

abstract class AbstractPathCalculator
{
  protected $graph;

  public function __construct(array $graph)
  {
    $this->graph = $graph;
  }

  public function getPath($fromCity, $toCity)
  {
    $dijkstra = new Dijkstra($this->graph);
    $paths = $dijkstra->shortestPaths($fromCity, $toCity);

    if(count($paths) < 1) {
      return [];
    }

    return $paths[0];
  }

  protected function getWeight($fromCity, $toCity)
  {
    $path = $this->getPath($fromCity, $toCity);

    if(empty($path)) {
      throw new \Exception("No path found");
    }

    $pathLength = count($path);
    $price = 0;
    for($i = 0; $i < $pathLength; $i++) {
      if($i < $pathLength - 1) {
        $price += $this->graph[$path[$i]][$path[$i+1]];
      }
    }

    return $price;
  }
}
