<?php

namespace Pauabus;

class FastestPathCalculator extends AbstractPathCalculator {
  public function getTime($fromCity, $toCity) {
    return $this->getWeight($fromCity, $toCity);
  }
}
